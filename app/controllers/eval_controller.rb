class EvalController < ApplicationController
  before_action :init_cms_vars

  def init_cms_vars
    @cms_site = Cms::Site.find_site(request.host, request.fullpath)
    # full_path and site are unique (CMS constraint in admin ui)
    @cms_page = Cms::Page.where(full_path: request.fullpath.gsub(@cms_site.path, '').squeeze('/')).where(site: @cms_site).first
  end
end